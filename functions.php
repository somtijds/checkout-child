<?php
/**
 * @package Checkout Child Theme
 */

/**
 * Load the parent and child theme styles
 */
function checkout_parent_theme_style() {

	// Parent theme styles
	wp_enqueue_style( 'checkout-style', get_template_directory_uri(). '/style.css' );

	// Child theme styles
	wp_enqueue_style( 'checkout-child-style', get_stylesheet_directory_uri(). '/style.css' );

	// JJ theme fonts
	wp_enqueue_style( 'jj-fonts-css', jj_fonts_url(), array(), null );
}
add_action( 'wp_enqueue_scripts', 'checkout_parent_theme_style',10 );

function jj_disable_checkout_fonts() {
	wp_dequeue_style('checkout-fonts');
}
add_action( 'wp_enqueue_scripts', 'jj_disable_checkout_fonts',20 );


function jj_change_array_portfolio_slug() {
	if ( class_exists( 'Array_Toolkit' ) ) {
		define('ARRAY_PORTFOLIO_ITEM_SLUG','courses');
	}
}
add_action( 'after_setup_theme', 'jj_change_array_portfolio_slug');


function jj_expand_thumbnail_support() {
	/**
	 * Enable support for Post Thumbnails
	 */
	remove_theme_support( 'post_thumbnails' );
	add_theme_support( 'post-thumbnails', array( 'post','page' ) );
}
add_action( 'after_setup_theme', 'jj_expand_thumbnail_support' );


/**
 * @param WP_Customize_Manager $wp_customize
 */
function jj_customizer_register( $wp_customize ) {

	/**
	 * Header and Footer Background Color
	 */
	$wp_customize->add_setting( 'jj_footer_color', array(
		'default'           => '#282E34',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jj_footer_color', array(
		'label'     => esc_html__( 'Footer Background Color', 'jj' ),
		'section'   => 'colors',
		'settings'  => 'jj_footer_color',
		'priority'  => 4,
	) ) );

	/**
	 * Header and Footer Background Color
	 */
	$wp_customize->add_setting( 'jj_article_link_color', array(
		'default'           => '#25AAE1',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jj_article_link_color', array(
		'label'     => esc_html__( 'Article link color', 'jj' ),
		'section'   => 'colors',
		'settings'  => 'jj_article_link_color',
		'priority'  => 2,
	) ) );


	/**
	 * Portfolio Section Background Color and Title
	 */
	$wp_customize->add_setting( 'jj_portfolio_section_background_color', array(
		'default'           => '#37BF91',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jj_portfolio_section_background_color', array(
		'label'           => esc_html__( 'Background Color', 'checkout' ),
		'section'         => 'checkout_portfolio_section',
		'panel'    		  => 'checkout_home_options',
		'settings'        => 'jj_portfolio_section_background_color',
		'priority'        => 11,
	) ) );

	$wp_customize->add_setting( 'jj_portfolio_title', array(
	'default'           => '',
	'type'              => 'option',
	'sanitize_callback' => 'checkout_sanitize_text',
	'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'jj_portfolio_title', array(
	'label'           => esc_html__( 'Portfolio Section Title', 'jj' ),
	'section'         => 'checkout_portfolio_section',
	'panel'           => 'checkout_home_options',
	'settings'        => 'jj_portfolio_title',
	'type'            => 'text',
	'priority'        => 1,
	) );

	/**
	 * Testimonials Section Image
	 */
	$wp_customize->add_setting( 'jj_testimonial_image', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'checkout_sanitize_text',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'jj_testimonial_image', array(
		'label'    => esc_html__( 'Testimonial Section Image', 'jj' ),
		'section'  => 'checkout_testimonial_section',
		'panel'    => 'checkout_home_options',
		'settings' => 'jj_testimonial_image',
		'priority' => 10,
	) ) );

	/**
	 * CTA Section Image
	 */
	$wp_customize->add_setting( 'jj_cta_image', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'checkout_sanitize_text',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'jj_cta_image', array(
		'label'    => esc_html__( 'Testimonial Section Image', 'jj' ),
		'section'  => 'checkout_footer_cta_section',
		'panel'    => 'checkout_home_options',
		'settings' => 'jj_cta_image',
		'priority' => 10,
	) ) );




}

add_action( 'customize_register', 'jj_customizer_register' );

function jj_custom_colors() {
	?>
	<style type="text/css">
		<?php if ( get_theme_mod( 'jj_footer_color' ) ) { ?>
			/* Background color for the header and footer */
			.site-footer {
				background-color: <?php echo get_theme_mod( 'jj_footer_color', '#2B3136' ) ;?>;
			}
		<?php } ?>

		<?php
			$nav_color = get_theme_mod( 'checkout_nav_color');
			 if ( ! empty( $nav_color ) && '#ffffff' === get_theme_mod( 'checkout_header_color' ) ) { ?>
			/* Link hover color for main navigation: don't turn into white when background is white  */
			.main-navigation a:hover,
			.main-navigation ul li.current-menu-item > a {
			  	color: <?php echo $nav_color; ?> !important;
			}
	  <?php } ?>

	  <?php 
	   if ( get_theme_mod( 'jj_portfolio_section_background_color' ) ) { ?>
			/* Background color for the portfolio section */
			.homepage-section--portfolio {
			  	background-color: <?php echo get_theme_mod( 'jj_portfolio_section_background_color' ); ?>;
			}
	  <?php } ?>

	  <?php
			 if ( get_theme_mod( 'jj_article_link_color' ) ) { ?>
			/* Link color for the header and footer */
			article a:not(.button) {
			  	color: <?php echo get_theme_mod( 'jj_article_link_color' ); ?>;
			}
			article a:not(.button):not(.post-featured-image):hover {
				color: <?php echo get_theme_mod( 'jj_article_link_color' ); ?>;
				border-bottom: 0.125em solid <?php echo get_theme_mod( 'jj_article_link_color' ); ?>;
			}
	  <?php } ?>

	  <?php
	  		$portfolio_num = get_option( 'checkout_homepage_portfolio_count' );
			 if ( ! empty( $portfolio_num ) && '2' === $portfolio_num ) { ?>
			/* With only two portfolio items, make the portfolio-wrapper smaller so it centers */
			 @media only screen and (min-width:960px) {
			    .homepage-section--portfolio .column {
			        width: 47%;
			    }

			    .homepage-section--portfolio .column:nth-child(3n) {
			        margin-right: 4%;
			    }

			    .homepage-section--portfolio .column:nth-child(2n) {
			        margin-right: 0;
			    }
			    .homepage-section--portfolio .portfolio-wrapper {
			    	max-width: 800px;
			    	margin-left: auto;
			    	margin-right: auto;
			    	display: block;
			    	overflow: hidden;
		    	}
		    }

	  <?php } ?>

	</style>
<?php
}
add_action( 'wp_head', 'jj_custom_colors', 11 );

/**
 * Return the Google font stylesheet URL
 *
 * @since checkout 1.0
 */
function jj_fonts_url() {

	$fonts_url = '';

	/* Translators: If there are characters in your language that are not
	 * supported by Patua One, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$patua = _x( 'on', 'Patua One font: on or off', 'jj' );

	/* Translators: If there are characters in your language that are not
	 * supported by Ubuntu, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$ubuntu = _x( 'on', 'Ubuntu font: on or off', 'jj' );

	if ( 'off' !== $patua || 'off' !== $ubuntu ) {
		$font_families = array();

		if ( 'off' !== $patua )
			$font_families[] = 'Patua One:400';

		if ( 'off' !== $ubuntu )
			$font_families[] = 'Ubuntu:400,700,400italic,700italic';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);
		$fonts_url = add_query_arg( $query_args, "//fonts.googleapis.com/css" );
	}

	return $fonts_url;
}

/**
 * Conditionally displays a metabox when used as a callback in the 'show_on_cb' cmb2_box parameter
 *
 * @param  CMB2 object $cmb CMB2 object
 *
 * @return bool             True if metabox should show
 */
function jj_show_if_front_page( $cmb ) {
	// Don't show this metabox if it's not the front page template
	if ( $cmb->object_id !== get_option( 'page_on_front' ) ) {
		return false;
	}
	return true;
}

add_action( 'cmb2_admin_init', 'jj_register_portfolio_metabox' );


function jj_register_portfolio_metabox() {
	$prefix = 'jj_portfolio_';

		/**
	 * SMetabox to demonstrate each field type included
	 */
	$jj_portfolio_meta = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'Pricing', 'jj' ),
		'object_types'  => array( 'array-portfolio', ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		// 'classes'    => 'extra-class', // Extra cmb2-wrap classes
		// 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
	) );

	$jj_portfolio_meta->add_field( array(
		'name'       => esc_html__( 'Pricing details for this item', 'jj' ),
		'desc'       => esc_html__( 'Add information about the costs of this course', 'jj' ),
		'id'         => $prefix . 'pricing_description',
		'type'       => 'textarea',
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
		// 'column'          => true, // Display field value in the admin post-listing columns
	) );
}

add_action( 'cmb2_admin_init', 'jj_register_usp_group_repeater_metabox' );
/**
 * Hook in and add a metabox to demonstrate repeatable grouped fields
 */
function jj_register_usp_group_repeater_metabox() {
	$prefix = 'jj_usp_group_';

	/**
	 * Repeatable Field Groups
	 */
	$jj_usp_box = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => esc_html__( 'Unique Selling Points', 'jj' ),
		'object_types' => array( 'page', ),
		'show_on_cb'   => 'jj_show_if_front_page',
	) );

	// $group_field_id is the field id string, so in this case: $prefix . 'repeater'
	$group_field_id = $jj_usp_box->add_field( array(
		'id'          => $prefix . 'repeater',
		'type'        => 'group',
		'description' => esc_html__( 'Generates Unique Selling Point Blocks', 'jj' ),
		'options'     => array(
			'group_title'   => esc_html__( 'USP {#}', 'jj' ), // {#} gets replaced by row number
			'add_button'    => esc_html__( 'Add Another USP', 'jj' ),
			'remove_button' => esc_html__( 'Remove USP', 'jj' ),
			'sortable'      => true, // beta
			// 'closed'     => true, // true to have the groups closed by default
		),
	) );

	/**
	 * Group fields works the same, except ids only need
	 * to be unique to the group. Prefix is not needed.
	 *
	 * The parent field's id needs to be passed as the first argument.
	 */
	$jj_usp_box->add_group_field( $group_field_id, array(
		'name'       => esc_html__( 'USP Title', 'jj' ),
		'id'         => 'title',
		'type'       => 'text',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	) );

	$jj_usp_box->add_group_field( $group_field_id, array(
		'name'        => esc_html__( 'Description', 'jj' ),
		'description' => esc_html__( 'Write a short description for this USP', 'jj' ),
		'id'          => 'description',
		'type'        => 'textarea_small',
	) );

	$jj_usp_box->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'USP Image', 'jj' ),
		'id'   => 'image',
		'type' => 'file',
	) );

}

/**
 * Add Editor role for theme options
 */
 $editor = get_role('editor');
 // add $cap capability to this role object
 $editor->add_cap('edit_theme_options');
