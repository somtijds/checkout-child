<?php
/**
 * The template used for displaying the USP items on the homepage.
 *
 * @package Checkout
 */
?>
		<?php do_action( 'jj_homepage_above_usp' ); ?>

		<div class="homepage-post-content">
			<?php foreach( $jj_usp as $usp ) : ?>

				<div class="jj-usp">
					<div class="jj-usp__image">
						<div class="jj-usp__image-border">
							<img src="<?php echo esc_url( $usp['image'] ); ?>">
						</div>
					</div>
					<div class="jj-usp__content">
						<header class="jj-usp__content__title">
							<h3><?php echo esc_html( $usp['title'] ); ?></h3>
						</header>
						<p class="jj-usp__content__description">
							<?php echo esc_html( $usp['description'] ); ?>
						</p>
					</div>
				</div>

			<?php endforeach; ?>
		</div>