<?php
/**
 * The template part for displaying the post meta information on portfolio items
 *
 * @package Checkout
 */

	$pricing = get_post_meta( get_the_ID(), 'jj_portfolio_pricing_description', true );
	if ( ! empty( $pricing ) ) : ?>

	<div class="jj-portfolio__extra">
		<ul>
		  <li>
		    <input type="checkbox" checked>
		    <a class="button">
		    	<?php echo _e('Pricing details','checkout'); ?>
		    	<i class="fa fa-angle-up"></i>
		    	<i class="fa fa-angle-down"></i>
		    </a>
		    <p><?php echo esc_html( $pricing ); ?></p>
		  </li>
		</ul>
	</div><!-- .portfolio-item__extra -->
<?php endif;

if ( class_exists( 'Array_Toolkit' ) ) {
	// Get the portfolio categories
	$portfolio_cats = get_the_term_list( get_the_ID(), 'categories', '', _x(' ', '', 'checkout' ), '' );
	// Get the portfolio tags
	$portfolio_tags = get_the_term_list( get_the_ID(), 'portfolio_tag', '', _x(' ', '', 'checkout' ), '' );
} else {
	$portfolio_cats = '';
	$portfolio_tags = '';
}
?>
	<div class="post-meta">
		<!-- Categories and tags for portfolio items -->
		<?php if ( $portfolio_cats || $portfolio_tags ) { ?>

			<span class="meta-cat">
				<?php echo $portfolio_cats; ?>

				<!-- Tags for portfolio items -->
				<?php if ( $portfolio_tags ) { ?>

					<span class="meta-tag">
						<?php echo $portfolio_tags; ?>
					</span>

				<?php } ?>
			</span>

		<?php } ?>

	</div><!-- .post-meta -->
