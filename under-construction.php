<!DOCTYPE html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>		 <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>		 <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Available soon: Juffrouw Janssen - Dutch courses for professionals</title>
		<meta property="og:title" content="Juffrouw Janssen - Dutch courses for professionals" />
		<meta property="og:description" content="This is where Somtijds is working on a little somethin' for Juffrouw Janssen." />
		<meta property="og:url" content="http://www.juffrouwjanssen.info" />
		<meta property="og:image" content="http://www.juffrouwjanssen.info/site/wp-content/uploads/2016/11/logo_Juffrouw-Janssen_def_optim.png" />
		<meta name="description" content="Under Construction: Juffrouw Janssen's new website">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">

	</head>
	<body class="noscroll">
		<!--[if lt IE 7]>
			<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

		<!-- Add your site or application content here -->
		<div class="uc-wrapper">
			<div class="uc">
				<div class="uc__content">
					<div class="text-wrapper">
						<div class="uc__description">
							<img src="http://www.juffrouwjanssen.info/site/wp-content/uploads/2016/11/logo_Juffrouw-Janssen_def_optim.png" alt="Juffrouw Janssen Logo" width="100%" />
							<p>This is where <a href="http://www.somtijds.nl" target="_blank">Somtijds</a> is working on a little somethin' for Juffrouw Janssen.</p>
						</div>
						<p itemscope itemtype="http://data-vocabulary.org/Person">
							<strong>
								<span itemprop="name">Juffrouw Janssen</span>
							</strong>
							<br />&#9758; <span itemprop="email"><a href="mailto:<?php echo eae_encode_str( 'lisa@juffrouwjanssen.info' ); ?>"><?php echo eae_encode_str( 'lisa@juffrouwjanssen.info' ); ?></a></span>
						</p>
						<p>
						&#9758; <span itemprop="linkedin"><a href="https://nl.linkedin.com/in/janssenlisa">Linkedin</a></span>
						<br />&#9758; <span itemprop="facebook"><a href="https://www.facebook.com/JuffrouwJanssenDutch">Facebook</a></span>
						</p>
					</div><!-- text-wrapper -->
				</div><!-- uc__content -->
			</div><!-- uc -->
		</div><!-- container -->
	</body>
</html>
