<?php
/**
 * Template Name: Homepage Portfolio
 *
 * This template displays a portfolio grid, a featured
 * portfolio item section, testimonials and a call-to-action banner.
 * Settings for this template can be found in Appearance ->
 * Customize -> Homepage Settings while viewing this page template.
 *
 * @package Checkout
 * @since Checkout 1.0
 */
get_header(); ?>

		<div id="main" class="site-main homepage-template">
			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">
				<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

						<!-- If there is a featured image, show it -->
						<?php if ( has_post_thumbnail() ) : ?>
							<div class="homepage-section-divider homepage-section-divider--image">
								<div class="image-border">
									<?php the_post_thumbnail( 'medium' ); ?>
								</div>
							</div>
						<?php endif; ?>

					<section class="homepage-section homepage-section--intro">
						<div class="homepage-section-inner">
						<?php do_action( 'checkout_homepage_above_content' ); ?>

						<!-- If there is post content, show it -->
						<?php if( get_the_content() ) : ?>

						<div class="homepage-post-content homepage-post-content--intro">
						<?php get_template_part( 'partials/content-home-intro' ); ?>
						</div>

						<?php endif; ?>

						<?php 
							
							$jj_usp = get_post_meta( get_the_id(), 'jj_usp_group_repeater', true);
							
							if ( ! empty( $jj_usp ) ) {
								include_once( locate_template( 'partials/content-usp-section.php' ) );
							}
						?>

						</div>
					</section>

					<?php endwhile; endif;
						?>
					
					<?php $portfolio_title = get_option( 'jj_portfolio_title' ); ?>

					<section 
						<?php if ($portfolio_title) : ?>
						id="<?php echo esc_attr( sanitize_title($portfolio_title) ); ?>"
						<?php endif; ?>
						class="homepage-section homepage-section--portfolio">
						<div class="homepage-section-inner">

						<?php do_action( 'checkout_homepage_above_portfolio' ); ?>
						<?php


						// Get number of portfolio items from customizer
						$portfolio_count = get_option( 'checkout_homepage_portfolio_count', '6' );

						$args = array(
							'post_type'      => 'array-portfolio',
							'posts_per_page' => (int) $portfolio_count,
						);
						$project_query = new WP_Query ( $args );
						if ( $project_query -> have_posts() ) :

						?>
							<?php 
								if ( $portfolio_title ) { ?>
								<h3 class="homepage-section__title homepage-section__title--portfolio">
									<?php echo esc_html( $portfolio_title ); ?>
								</h3>
							<?php } ?>

							<div itemscope class="portfolio-wrapper download-wrapper">

								<?php while ( $project_query -> have_posts() ) : $project_query -> the_post();

									get_template_part( 'partials/content-portfolio-thumbs' );

								endwhile; ?>

							</div><!-- .portfolio-wrapper -->

						<?php endif; ?>
						<?php wp_reset_query(); ?>

						<?php do_action( 'checkout_homepage_above_split' ); ?>


						<!-- Featured Portfolio items -->
						<?php
							$featured_tag = get_theme_mod( 'checkout_portfolio_tag_select', '' );
							if ( '' !== $featured_tag ) {

								$featured_args = array(
									'post_type'      => 'array-portfolio',
									'posts_per_page' => 6,
									'no_found_rows'  => true,
									'tax_query'      => array(
										array(
											'taxonomy' => 'portfolio_tag',
											'field'    => 'slug',
											'terms'    => $featured_tag
										),
									),
								);
								$featured_query = new WP_Query ( $featured_args );
								if ( $featured_query -> have_posts() ) :
							?>

								<section class="split-section">
									<ul class="rslides product-slides">
										<?php while ( $featured_query -> have_posts() ) : $featured_query -> the_post();
											get_template_part( 'partials/content-featured-product' );
										endwhile; ?>
									</ul>

									<ul id="product-pager">
										<?php while ( $featured_query -> have_posts() ) : $featured_query -> the_post(); ?>

												<li><a class="paging-thumb" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark">
													<?php if ( has_post_thumbnail() ) {
														the_post_thumbnail( 'paging-thumb' );
													} else { ?>
														<img src="<?php echo get_template_directory_uri(); ?>/images/default-thumb.jpg" alt="placeholder" />
													<?php } ?>
												</a></li>

										<?php endwhile; ?>
									</ul>
								</section><!-- .split-section -->

							<?php endif; ?>
							<?php wp_reset_query(); ?>
						<?php } // If featured_tag exists ?>
						</div>
					</section>

					<!-- Get the call-to-action section -->
					<?php get_template_part( 'partials/content-cta-section' ); ?>

					<!-- Get the testimonial section -->
					<?php get_template_part( 'partials/content-testimonial-section' ); ?>

				</div><!-- #content .site-content -->
			</div><!-- #primary .content-area -->
		</div><!-- #main .site-main -->

		

<?php get_footer(); ?>